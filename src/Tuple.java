public class Tuple {
    public int x;
    public int y;

    public Tuple(int x,int y){
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Tuple) {
            if (( ((Tuple) obj).x ==x && ((Tuple) obj).y==y)
                || ( ((Tuple) obj).x ==y && ((Tuple) obj).y==x)){
                return true;
            }
            else{
                return false;
            }
        }
        return false;
    }
}
