import java.util.ArrayList;

public class IranMapGraph extends MapGraph{

    public IranMapGraph(){
        createProvinces();
        createConnections();
        numberConnections = connection.size();
        numberProvinces = province.length;
    }

    private void createProvinces(){
        province = new String[] {
                "West Azerbaijan",              //0
                "East Aazerbaijan",             //1
                "Ardabil",                      //2
                "Kurdistan",                    //3
                "Zanjan",                       //4
                "Gilan",                        //5
                "Kermanshah",                   //6
                "Hamedan",                      //7
                "Qazvin",                       //8
                "Mazandaran",                   //9
                "Alborz",                       //10
                "Tehran",                       //11
                "Qom",                          //12
                "Markazi",                      //13
                "Lorestan",                     //14
                "Ilam",                         //15
                "Khozestan",                    //16
                "Chaharmahal and Bakhtiari",    //17
                "Kohgiluyeh and Boyer-Ahmad",   //18
                "Isfehan",                      //19
                "Semnan",                       //20
                "Golestan",                     //21
                "North Khorasan",               //22
                "Razavi Khorasan",              //23
                "South Khorasan",               //24
                "Yazd",                         //25
                "Fars",                         //26
                "Bushehr",                      //27
                "Hormozgan",                    //28
                "Kerman",                       //29
                "Sistan and Baluchestan"        //30
        };

    }

    private void createConnections(){
        connection = new ArrayList<Tuple>();
        connection.add(new Tuple(0,1));
        connection.add(new Tuple(0,3));
        connection.add(new Tuple(0,4));

        connection.add(new Tuple(1,2));
        connection.add(new Tuple(1,4));

        connection.add(new Tuple(2,4));
        connection.add(new Tuple(2,5));

        connection.add(new Tuple(3,4));
        connection.add(new Tuple(3,6));
        connection.add(new Tuple(3,7));

        connection.add(new Tuple(4,5));
        connection.add(new Tuple(4,7));
        connection.add(new Tuple(4,8));

        connection.add(new Tuple(5,8));
        connection.add(new Tuple(5,9));

        connection.add(new Tuple(6,7));
        connection.add(new Tuple(6,14));
        connection.add(new Tuple(6,15));

        connection.add(new Tuple(7,8));
        connection.add(new Tuple(7,13));
        connection.add(new Tuple(7,14));

        connection.add(new Tuple(8,9));
        connection.add(new Tuple(8,10));
        connection.add(new Tuple(8,13));

        connection.add(new Tuple(9,10));
        connection.add(new Tuple(9,11));
        connection.add(new Tuple(9,20));
        connection.add(new Tuple(9,21));

        connection.add(new Tuple(10,11));
        connection.add(new Tuple(10,13));

        connection.add(new Tuple(11,12));
        connection.add(new Tuple(11,13));
        connection.add(new Tuple(11,20));

        connection.add(new Tuple(12,13));
        connection.add(new Tuple(12,19));
        connection.add(new Tuple(12,20));

        connection.add(new Tuple(13,14));
        connection.add(new Tuple(13,19));

        connection.add(new Tuple(14,15));
        connection.add(new Tuple(14,16));
        connection.add(new Tuple(14,17));
        connection.add(new Tuple(14,19));

        connection.add(new Tuple(15,16));

        connection.add(new Tuple(16,17));
        connection.add(new Tuple(16,18));
        connection.add(new Tuple(16,27));

        connection.add(new Tuple(17,18));
        connection.add(new Tuple(17,19));

        connection.add(new Tuple(18,19));
        connection.add(new Tuple(18,26));
        connection.add(new Tuple(18,27));

        connection.add(new Tuple(19,20));
        connection.add(new Tuple(19,24));
        connection.add(new Tuple(19,25));
        connection.add(new Tuple(19,26));

        connection.add(new Tuple(20,21));
        connection.add(new Tuple(20,22));
        connection.add(new Tuple(20,23));
        connection.add(new Tuple(20,24));

        connection.add(new Tuple(21,22));

        connection.add(new Tuple(22,23));

        connection.add(new Tuple(23,24));

        connection.add(new Tuple(24,25));
        connection.add(new Tuple(24,29));
        connection.add(new Tuple(24,30));

        connection.add(new Tuple(25,26));
        connection.add(new Tuple(25,29));

        connection.add(new Tuple(26,27));
        connection.add(new Tuple(26,28));
        connection.add(new Tuple(26,29));

        connection.add(new Tuple(27,28));

        connection.add(new Tuple(28,29));
        connection.add(new Tuple(28,30));

        connection.add(new Tuple(29,30));

    }

}
