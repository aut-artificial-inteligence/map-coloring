public class SimulatedAnnealingSolver {
    private char[] colors;
    private MapGraph map;
    private char[] state;

    private double t;
    private double t0 = 1000;
    private int k = 0;

    private final double ALPHA1 = 0.85;
    private final double ALPHA2 = 10000;
    private final double ALPHA3 = 100;
    private final double ALPHA4 = 100;

    private int temperatureFunctionSelect;
    public SimulatedAnnealingSolver(MapGraph map, char[] colors,int temperatureFunctionSelect){
        this.map = map;
        this.colors = colors;
        this.temperatureFunctionSelect = temperatureFunctionSelect;
    }

    public void solve(){
        initializeState();
        while(!isGoal(state)){
            int chosenGen = (int)(Math.random()*map.numberOfNodes());
            char chosenColor = colors[(int)(Math.random()*colors.length)];
            if(state[chosenGen] != chosenColor){
                char[] nextState = new char[map.numberOfNodes()];
                System.arraycopy(state,0,nextState,0,map.numberOfNodes());
                nextState[chosenGen] = chosenColor;
                double currentFitness = fitnessFunction(state);
                double nextFitness = fitnessFunction(nextState);
                if(nextFitness >= currentFitness){
                    state = nextState;
                    k++;
                    nextTemperature();
                    System.out.println(fitnessFunction(state) + " " + t +" " + k);

                }
                else{
                    double deltaE = nextFitness-currentFitness;
                    double p = Math.exp(deltaE/t);
                    if(Math.random()<p){
                        state = nextState;
                        k++;
                        nextTemperature();
                      //  System.out.println(fitnessFunction(state) + " " + t +" " + k);

                    }
                }
            }
        }
        showState(state);
    }

    private void showState(char[] state){
        for(int i=0 ; i<map.numberOfNodes() ; i++){
            System.out.println(map.getNodeName(i)+" : "+state[i]);
        }
        System.out.println("=========================");
    }

    private void nextTemperature(){
        switch (temperatureFunctionSelect){
            case 1:
                t = t0*Math.pow(ALPHA1,k);
                break;
            case 2:
                t = t0 / ( 1 + ALPHA2 * Math.log10(1+k));
                break;
            case 3:
                t = t0 / ( 1 + ALPHA3 * k);
                break;
            case 4:
                t = t0 / ( 1 + ALPHA4*k*k);
                break;
        }
    }

    public void initializeState(){
        state = new char[map.numberOfNodes()];
        for(int i=0 ; i<map.numberOfNodes() ; i++){
            char chosenColor = colors[(int)(Math.random()*colors.length)];
            state[i] = chosenColor;
        }
    }

    public boolean isGoal(char[] state){
        for(int i=0 ; i<map.numberOfNodes() ; i++){
            for(int j=i+1 ; j<map.numberOfNodes() ; j++){
                if(map.isConnect(i,j) && state[i]==state[j]){
                    return false;
                }
            }
        }
        return true;
    }

    private double fitnessFunction(char[] state){
        int m = map.numberOfEdges();
        double fitness = 0;
        for(int i=0 ; i<map.numberOfNodes() ; i++){
            for(int j=i+1 ; j<map.numberOfNodes() ; j++){
                if(map.isConnect(i,j)){
                    if(state[i]!=state[j]){
                        fitness++;
                    }
                }
            }
        }
        fitness*=2;
        fitness/=m;
        return fitness;
    }
}
