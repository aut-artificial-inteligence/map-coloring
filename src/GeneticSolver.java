import java.util.ArrayList;
import java.util.Collections;

public class GeneticSolver {
    private final int NUMBER_OF_GENERATION = 5000;
    private final int POPULATION_SIZE = 100;
    private final int TORNUMENT_SIZE = 5;
    private final double MUTATION_RATE = 0.1;
    private final int PARENT_SIZE = POPULATION_SIZE/TORNUMENT_SIZE;


    private char[] colors;
    private MapGraph map;

    private char[][] population;
    private char[][] parent;

    public GeneticSolver(MapGraph map, char[] colors){
        this.map = map;
        this.colors = colors;
        parent = new char[PARENT_SIZE][map.numberOfNodes()];
    }

    public void solve(){
        createPopulation();
        char[] goalState;
        showFitnessStatus();
        for(int i=0 ; i<NUMBER_OF_GENERATION ; i++){
           // goalState = goalState();
           // if(goalState==null){
                chooseParent();
                nextPopulation();
                mutation();
         /*   }
            else{
                System.out.println("\nSolver Succeed!");
                showState(goalState);
                return;
            }*/
            showFitnessStatus();
        }
        char[] bestState = bestState();
      //  System.out.println("\nSolver can not find Goal!");
        System.out.println("Fitness: "+fitnessFunction(bestState));
        showState(bestState);
    }

    private void showState(char[] state){
        for(int i=0 ; i<map.numberOfNodes() ; i++){
            System.out.println(map.getNodeName(i)+" : "+state[i]);
        }
        System.out.println("=========================");
    }

    private void mutation(){
        int mutatedGenomes = (int) (POPULATION_SIZE*map.numberOfNodes()*MUTATION_RATE);
        for(int i=0 ; i<mutatedGenomes ; i++){
            int chosenChromosome = (int)(Math.random()*POPULATION_SIZE);
            int chosenGen = (int)(Math.random()*map.numberOfNodes());
            int chosenColor = (int)(Math.random()*colors.length);
            if(chromosome(chosenChromosome)[chosenGen]==chosenColor){
                i--;
            }
            else{
                population[chosenChromosome][chosenGen] = colors[chosenColor];
            }
        }
    }

    private void nextPopulation(){

        for(int i=0 ; i<POPULATION_SIZE ; i++){
            int firstParent = (int)(Math.random()*PARENT_SIZE);
            int secondParent = (int)(Math.random()*PARENT_SIZE);
            if(firstParent==secondParent){
                i--;
            }
            else{
                population[i] = crossover(firstParent,secondParent);
            }

        }

    }

    private char[] crossover(int firstParent, int secondParent){
        char[] child = new char[map.numberOfNodes()];
        for(int i=0 ; i<map.numberOfNodes()/2 ; i++){
            child[i] = parent[firstParent][i];
        }

        for(int i=map.numberOfNodes()/2 ; i<map.numberOfNodes(); i++){
            child[i] = parent[secondParent][i];
        }
        return child;
    }

    private void chooseParent(){
        ArrayList<Integer> chromosomeOrder = new ArrayList<Integer>();
        for (int i=0; i<POPULATION_SIZE; i++) {
            chromosomeOrder.add(i);
        }

        Collections.shuffle(chromosomeOrder);

        for(int group=0 ; group<PARENT_SIZE ; group++){
            int []groupChromosome = new int[TORNUMENT_SIZE];
            for(int i=0 ; i<TORNUMENT_SIZE ; i++){
                groupChromosome[i] = chromosomeOrder.get(group*TORNUMENT_SIZE + i);
            }

            int bestChromosome = groupChromosome[0];
            double bestFitness = fitnessFunction(chromosome(bestChromosome));
            for(int i = 1 ; i<TORNUMENT_SIZE ; i++){
                int newChromosome = groupChromosome[i];
                double newFitness = fitnessFunction(chromosome(newChromosome));
                if(newFitness>bestFitness){
                    bestFitness = newFitness;
                    bestChromosome = newChromosome;
                }
            }
            parent[group] = chromosome(bestChromosome);
        }
    }


    private void createPopulation(){
        population = new char[POPULATION_SIZE][map.numberOfNodes()];
        for(int i=0 ; i<POPULATION_SIZE ; i++){
            for(int j=0 ; j<map.numberOfNodes() ; j++){
                char randomColor = colors[(int)(Math.random()*colors.length)];
                population[i][j] = randomColor;
            }
        }
    }

    private char[] chromosome(int number){
        return population[number];
    }

    private void showFitnessStatus(){
        double sumFitness = 0;
        double bestFitness = -1;
        double worstFitness = +3;
        double meanFitness;

        for(int i=0 ; i<POPULATION_SIZE ; i++){
            double fit = fitnessFunction(population[i]);
            sumFitness += fit;
            if(fit<worstFitness){
                worstFitness = fit;
            }
            if(fit>bestFitness){
                bestFitness = fit;
            }
        }
        meanFitness = sumFitness/POPULATION_SIZE;
        System.out.println(bestFitness+"\t"+worstFitness+"\t"+meanFitness);
    }

    private double fitnessFunction(char[] state){
        int m = map.numberOfEdges();
        double fitness = 0;
        for(int i=0 ; i<map.numberOfNodes() ; i++){
            for(int j=i+1 ; j<map.numberOfNodes() ; j++){
                if(map.isConnect(i,j)){
                    if(state[i]!=state[j]){
                        fitness++;
                    }
                }
            }
        }
        fitness*=2;
        fitness/=m;
        return fitness;
    }

    private char[] goalState(){
        char[] bestState = bestState();
        if(fitnessFunction(bestState)==2){
            return bestState;
        }
        else {
            return null;
        }
    }

    private char[] bestState(){
        int bestChromosome = 0;
        double bestFitness = fitnessFunction(chromosome(0));
        for(int i=1 ; i<POPULATION_SIZE ; i++){
            int newChromosome = i;
            double newFitness = fitnessFunction(chromosome(newChromosome));
            if(newFitness>bestFitness){
                bestFitness = newFitness;
                bestChromosome = newChromosome;
            }
        }
        return chromosome(bestChromosome);
    }
}
