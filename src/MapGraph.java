import java.util.ArrayList;

public class MapGraph{
    protected static ArrayList<Tuple> connection = null;
    protected int numberConnections;
    protected int numberProvinces;
    protected String province[];

    public MapGraph(ArrayList<Tuple> connection,String[] province){
        this.connection = connection;
        this.province = province;
        numberConnections = connection.size();
        numberProvinces = province.length;
    }

    public MapGraph(){

    }

    public boolean isConnect(int p1,int p2){
        Tuple tuple = new Tuple(p1,p2);
        if (connection.contains(tuple)){
            return true;
        }
        else{
            return false;
        }
    }

    public String getNodeName(int number){
        return province[number];
    }

    public int numberOfEdges(){
        return numberConnections;
    }

    public int numberOfNodes(){
        return numberProvinces;
    }

}