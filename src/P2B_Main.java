public class P2B_Main {
    public static void main(String args[]){
        IranMapGraph map = new IranMapGraph();
        char[] colors = new char[]{'R','G','B','Y'};
        SimulatedAnnealingSolver solver = new SimulatedAnnealingSolver(map,colors,4);
        solver.solve();
    }
}
