public class P2A_Main {
    public static void main(String args[]){
        IranMapGraph map = new IranMapGraph();
        char[] colors = new char[]{'R','G','B','Y'};
        GeneticSolver solver = new GeneticSolver(map,colors);
        solver.solve();
    }
}
