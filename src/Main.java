import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

/*
        MapGraph map = new IranMapGraph();
        char[] colors = new char[]{'R','G','B','Y'};
        GeneticSolver solver = new GeneticSolver(map,colors);
        solver.solve();
*/

        /*
        String province[] = new String[]{"Kashan","Tehran","Qom","Esfehan"};
        char[] colors = new char[]{'R','G','B','Y'};
        ArrayList<Tuple> connection = new ArrayList<Tuple>();
        connection.add(new Tuple(0,2));
        connection.add(new Tuple(0,3));
        connection.add(new Tuple(1,2));
        MapGraph map = new MapGraph(connection, province);
        GeneticSolver solver = new GeneticSolver(map,colors);
        solver.solve();*/

/*
        IranMapGraph map = new IranMapGraph();
        char[] colors = new char[]{'R','G','B','Y'};
        SimulatedAnnealingSolver solver = new SimulatedAnnealingSolver(map,colors,1);
        solver.solve();
  */

        ArrayList<Tuple> connection = new ArrayList<Tuple>();
        connection.add(new Tuple(0,1));
        connection.add(new Tuple(0,4));
        connection.add(new Tuple(1,2));
        connection.add(new Tuple(1,4));
        connection.add(new Tuple(2,3));
        connection.add(new Tuple(2,4));
        connection.add(new Tuple(3,4));

        String[] provience = new String[]{"V0", "V1","V2","V3","V4"};

        char[] colors = new char[]{'R','G','B'};


        MapGraph map = new MapGraph(connection,provience);
        SimulatedAnnealingSolver solver = new SimulatedAnnealingSolver(map,colors,1);
        solver.solve();
    }



}
